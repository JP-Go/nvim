local M = {}

local bind_keymapper = function(mode, pre_opts)
    pre_opts = pre_opts or { noremap = true }
    return function(lhs, rhs, opts)
        opts = vim.tbl_extend('force', pre_opts, opts or {})
        vim.keymap.set(mode, lhs, rhs, opts)
    end
end

M.nmap = bind_keymapper('n', { noremap = false })
M.nnoremap = bind_keymapper('n')
M.xnoremap = bind_keymapper('x')
M.vnoremap = bind_keymapper('v')
M.inoremap = bind_keymapper('i')
M.snoremap = bind_keymapper('s')
M.tnoremap = bind_keymapper('t')

return M
