return {
  'akinsho/toggleterm.nvim',
  lazy = false,
  config = {
    open_mapping = [[<C-t>]],
  },
}
