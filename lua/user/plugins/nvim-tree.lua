return {
  'nvim-neo-tree/neo-tree.nvim',
  branch = 'v2.x',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'nvim-tree/nvim-web-devicons', -- not strictly required, but recommended
    'MunifTanjim/nui.nvim',
  },

  config = {
    filesystem = {
      filtered_items = {
        visible = true,
      },
    },
  },
  keys = {
    { '<leader>e', '<cmd>Neotree toggle<CR>', desc = '[E]xplorer' },
  },
}
