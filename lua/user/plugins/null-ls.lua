return {
  'jose-elias-alvarez/null-ls.nvim',
  lazy = false,
  config = function()
    local null_ls = require('null-ls')

    local augroup = vim.api.nvim_create_augroup('LspFormatting', {})

    null_ls.setup({
      on_attach = function(client, bufnr)
        if client.supports_method('textDocument/formatting') then
          vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
          vim.api.nvim_create_autocmd('BufWritePre', {
            group = augroup,
            buffer = bufnr,
            callback = function()
              vim.lsp.buf.format({
                bufnr = bufnr,
                filter = function(format_client)
                  return format_client.name == 'null-ls'
                end,
              })
            end,
          })
        end
      end,
      debug = true,
      sources = {
        null_ls.builtins.formatting.stylua.with({
          extra_args = { '-f', '/home/jp/.config/stylua.toml' },
        }),
        null_ls.builtins.formatting.gofmt,
        null_ls.builtins.formatting.clang_format,
        null_ls.builtins.formatting.prettierd.with({
          filetypes = {
            'javascript',
            'javascriptreact',
            'typescript',
            'typescriptreact',
            'json',
            'html',
            'css',
          },
        }),
      },
    })
  end,
} -- Linters | Formatters manager
