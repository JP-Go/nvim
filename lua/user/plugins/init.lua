local plugins = {
  -- Colorschemes
  { 'tpope/vim-fugitive', cmd = 'Git' },
  {
    "rebelot/kanagawa.nvim"
  },
  {
    'sainnhe/sonokai',
  },
  { 'savq/melange' },
  -- Autopairs
  {
    'echasnovski/mini.pairs',
    config = function(_, opts)
      require('mini.pairs').setup(opts)
    end,
  },
  -- Surround
  {
    'echasnovski/mini.surround',
    config = function(_, opts)
      require('mini.surround').setup(opts)
    end,
  },
  { 'mfussenegger/nvim-jdtls', ft = 'java' }, -- Jdtls wrapper
  -- Indent Guides
  {
    'lukas-reineke/indent-blankline.nvim',
    config = true,
    opts = {
      show_current_context = true,
    },
  },
  {
    'nvim-lualine/lualine.nvim',
    config = true,
  }, -- StatusLine
  {
    'akinsho/bufferline.nvim',
    tag = 'v3.1.0',
    config = true,
    lazy = false,
    keys = {
      { '<S-h>', '<cmd>BufferLineCyclePrev<CR>', desc = 'Previous buffer' },
      { '<S-l>', '<cmd>BufferLineCycleNext<CR>', desc = 'Next buffer' },
    },
  },
  {
    'TimUntersberger/neogit',
    dependencies = { 'nvim-lua/plenary.nvim' },
    keys = {
      {
        '<leader>gg',
        '<cmd>Neogit<CR>',
        desc = '[G]o [G]it',
      },
    },
  },
  { 'numToStr/Comment.nvim', config = true }, -- Commenter
  -- Lsp signature helpers
  {
    'ray-x/lsp_signature.nvim',
    config = true,
    opts = { bind = true, floating_window = false },
  },
  --  Auto tag for some files
  { 'windwp/nvim-ts-autotag', config = true },
  -- Multi cursor (cursed, I know.)
  { 'mg979/vim-visual-multi' },
}
return plugins
