return {
  'nvim-treesitter/nvim-treesitter',
  build = function()
    require('nvim-treesitter.install').update({ with_sync = true })
  end,
  config = function()
    require('nvim-treesitter.configs').setup({
      ensure_installed = {
        'c',
        'lua',
        'rust',
        'prisma',
        'javascript',
        'typescript',
        'python',
        'bash',
        'css',
        'tsx',
        'graphql',
        'html',
        'haskell',
      },
      sync_install = false,

      highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
      },
    })
  end,
}
