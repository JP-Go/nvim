return {
  'nvim-telescope/telescope.nvim',
  branch = '0.1.x',
  dependencies = { 'nvim-lua/plenary.nvim' },
  config = true,
  keys = {

    {
      '<leader>ff',
      '<cmd>Telescope find_files<CR>',
      desc = '[F]ind [F]iles',
    },
    {
      '<leader>fg',
      '<cmd>Telescope git_files<CR>',
      desc = '[F]ind [G]it Files'
    },
    {
      '<leader>sb',
      '<cmd>Telescope buffers<CR>',
      desc = '[S]earch [B]uffers',
    },
    { '<leader>h', '<cmd>Telescope help_tags<CR>', desc = '[H]elp' },
    {
      '<leader>sk',
      '<cmd>Telescope keymaps<CR>',
      desc = '[S]earch [K]eymaps',
    },
    { '<leader>st', '<cmd>Telescope <CR>', desc = '[S]earch [T]elescope' },

    {
      '<leader>sg',
      '<cmd>Telescope live_grep<CR>',
      desc = '[S]earch with [G]rep',
    },
  },
}
