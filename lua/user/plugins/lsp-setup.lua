return {
  'VonHeikemen/lsp-zero.nvim',
  lazy = false,
  dependencies = {
    'neovim/nvim-lspconfig',
    'williamboman/mason.nvim',
    'williamboman/mason-lspconfig.nvim',
    -- Autocompletion
    'hrsh7th/nvim-cmp',
    'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-path',
    'hrsh7th/cmp-cmdline',
    'saadparwaiz1/cmp_luasnip',
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-nvim-lua',
    -- Snippets
    'L3MON4D3/LuaSnip',
    'rafamadriz/friendly-snippets',
  },
  config = function()
    local lsp = require('lsp-zero')

    lsp.preset('recommended')
    lsp.ensure_installed({
      'tsserver',
      -- 'lua_ls',
      'pyright',
      'texlab',
      'jdtls',
      'emmet_ls',
      'cssls',
    })

    local icons = {
      File = ' ',
      Module = ' ',
      Namespace = ' ',
      Package = ' ',
      Class = ' ',
      Method = ' ',
      Property = ' ',
      Field = ' ',
      Constructor = ' ',
      Enum = ' ',
      Interface = ' ',
      Function = ' ',
      Variable = ' ',
      Constant = ' ',
      String = ' ',
      Number = ' ',
      Boolean = ' ',
      Array = ' ',
      Object = ' ',
      Key = ' ',
      Null = ' ',
      EnumMember = ' ',
      Struct = ' ',
      Event = ' ',
      Snippet = ' ',
      Operator = ' ',
      TypeParameter = ' ',
    }

    local cmp = require('cmp')
    local cmp_mappings = lsp.defaults.cmp_mappings({
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<CR>'] = cmp.mapping.confirm({ select = true }),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<C-Space>'] = cmp.mapping.complete(),
    })

    lsp.setup_nvim_cmp({
      -- completion = {
      --   completeopt = { 'menu', 'menuone', 'noinsert' },
      -- },
      mapping = cmp_mappings,
      sources = {
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
        { name = 'path' },
        { name = 'buffer', keyword_length = 5 },
      },
      formatting = {
        fields = { 'abbr', 'kind' },
        format = function(_, vim_item)
          vim_item.kind =
            string.format('%s %s', icons[vim_item.kind] or '', vim_item.kind)
          return vim_item
        end,
      },
    })

    cmp.setup.cmdline({ '/', '?' }, {
      mapping = cmp.mapping.preset.cmdline(),
      sources = {
        { name = 'buffer' },
      },
    })

    -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
    cmp.setup.cmdline(':', {
      mapping = cmp.mapping.preset.cmdline(),
      sources = cmp.config.sources({
        { name = 'path' },
      }, {
        { name = 'cmdline' },
      }),
    })

    lsp.set_preferences({
      suggest_lsp_servers = false,
      sign_icons = {
        error = '',
        warn = '⚠',
        hint = '!',
        info = '',
      },
    })

    lsp.on_attach(function(_, bufnr)
      local nmap = function(lhs, rhs, opts)
        vim.keymap.set('n', lhs, rhs, opts)
      end
      local imap = function(lhs, rhs, opts)
        vim.keymap.set('i', lhs, rhs, opts)
      end
      nmap(
        'gd',
        vim.lsp.buf.definition,
        { buffer = bufnr, remap = false, desc = '[G]o to [d]efinition' }
      )
      nmap(
        'K',
        vim.lsp.buf.hover,
        { buffer = bufnr, remap = false, desc = 'Hover documentation' }
      )
      nmap(
        '<leader>ws',
        vim.lsp.buf.workspace_symbol,
        { buffer = bufnr, remap = false, desc = '[W]orkspace [S]ymbols' }
      )
      nmap(
        ']d',
        vim.diagnostic.goto_next,
        { buffer = bufnr, remap = false, desc = 'Next [d]iagnostic' }
      )
      nmap(
        '[d',
        vim.diagnostic.goto_prev,
        { buffer = bufnr, remap = false, desc = 'Previous [d]iagnostic' }
      )
      nmap(
        '<leader>ca',
        vim.lsp.buf.code_action,
        { buffer = bufnr, remap = false, desc = '[C]ode [a]ction' }
      )
      nmap(
        '<leader>gr',
        vim.lsp.buf.references,
        { buffer = bufnr, remap = false, desc = '[G]o to [r]eferences' }
      )
      nmap(
        '<leader>r',
        vim.lsp.buf.rename,
        { buffer = bufnr, remap = false, desc = '[R]nename' }
      )
      imap(
        '<C-k>',
        vim.lsp.buf.signature_help,
        { buffer = bufnr, remap = false, desc = 'Signature help' }
      )
    end)

    lsp.configure('emmet_ls', require('user.servers_configs.emmet_ls'))

    lsp.setup()


  end,
}
