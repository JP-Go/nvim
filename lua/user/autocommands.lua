vim.api.nvim_create_augroup('QLI', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  pattern = '*',
  callback = function()
    vim.highlight.on_yank({ timeout = 100 })
  end,
  desc = 'Highlights the selected text on yank',
  group = 'QLI',
})

vim.api.nvim_create_autocmd('BufEnter', {
  pattern = '*',
  command = 'set formatoptions-=cro',
  desc = 'Remove cro from formatoptions (check :h formatoptions)',
  group = 'QLI',
})

vim.api.nvim_create_autocmd('BufWritePost', {
  pattern = 'plugins.lua',
  command = 'so % | PackerSync',
  group = 'QLI',
})
