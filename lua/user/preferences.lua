local options = {
  relativenumber = true,
  number = true,
  termguicolors = true,
  mouse = '',
  background = 'dark',
  laststatus = 3,
  tabstop = 4,
  shiftwidth = 0,
  softtabstop = 0,
  expandtab = true,
  swapfile = false,
  backup = false,
  undofile = true,
  hlsearch = false,
  scrolloff = 10,
  foldmethod = 'expr',
  foldexpr = 'nvim_treesitter#foldexpr()',
  foldenable = false,
  signcolumn = 'yes',
  cursorline = true,
  cmdheight = 2,
}

for k, v in pairs(options) do
  vim.opt[k] = v
end
