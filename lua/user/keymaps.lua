local nnoremap = require('user.utils').nnoremap
local inoremap = require('user.utils').inoremap
local vnoremap = require('user.utils').vnoremap
local tnoremap = require('user.utils').tnoremap

--- General keymaps ---
-- Space as leader key
vim.g.mapleader = ' '

-- Normal mode keymaps
nnoremap('<leader>d', '<cmd>bdelete<CR>', { desc = '[D]elete buffer' })
nnoremap('<leader>sf', '<cmd>source %<CR>', { desc = '[S]ource [F]ile' })
nnoremap('<C-l>', '<C-w>l', { desc = 'Right Split' })
nnoremap('<C-h>', '<C-w>h', { desc = 'Left Split' })

-- Yanking and pasting
nnoremap('<leader>p', '"+p', { desc = '[P]aste forward from clipboard' })
vnoremap('<leader>p', '"+p', { desc = '[P]aste forward from clipboard' })

nnoremap('<leader>P', '"+P', { desc = '[P]aste backward from clipboard' })
vnoremap('<leader>P', '"+P', { desc = '[P]aste backward from clipboard' })

nnoremap('<leader>y', '"+y', { desc = '[Y]ank forward from clipboard' })
vnoremap('<leader>y', '"+y', { desc = '[Y]ank forward from clipboard' })

-- Insert mode keymaps
inoremap('', '<C-w>', { desc = 'Delete previous word' })

-- Visual mode keymaps
vnoremap('<', '<gv', { desc = 'Indent in|left' })
vnoremap('>', '>gv', { desc = 'Ident out|right' })

-- Terminal mode keymaps
tnoremap('<Esc>', '<C-\\><C-n>', { desc = 'Exit terminal mode' })
